# Hotel Booking System

RESTful API on Java using Spring. Architecture of the “Controller-Service-Repository” application with responses in the form of a JSON format and many-to-one relationships.

The purpose of the Hotel Booking System is to allows users to view, reserve rooms in hotels and provide feedback about their stay.

## Technologies
* Java, Spring Boot, REST API
* JPA (Hibernate), PostgresSQL
* JUnit 5, Mockito, Maven
* IntelliJ IDEA Ultimate thanks to [OpenSourceSupport](https://www.jetbrains.com/community/opensource/?utm_campaign=opensource&utm_content=approved&utm_medium=email&utm_source=newsletter&utm_term=jblogo#support).

## Features
Each entity is provided with CRUD methods:
* Hotel: Provides rooms for accommodation.
* Room: An individual unit within a hotel meant for accommodation.
* User: Represents an individual user with concrete role and an encrypted password.
* Reservation: Captures the details of a room booked by a user for a specific period.
* Review: Allows users to provide feedback on their stays.

### How to use
App builds on clear backend, you don't found any frontend part. Functional was checked using API Endpoints. Instructions for it:
1. Run the app.
2. Open database (PgAdmin 4).
3. Send request in Postman.