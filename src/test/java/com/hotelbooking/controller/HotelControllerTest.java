package com.hotelbooking.controller;

import com.hotelbooking.model.Hotel;
import com.hotelbooking.service.HotelService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class HotelControllerTest {

    @InjectMocks
    private HotelController hotelController;

    @Mock
    private HotelService hotelService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllHotels() {
        Hotel hotel = new Hotel();
        hotel.setName("Test Hotel");
        when(hotelService.getAllHotels()).thenReturn(Collections.singletonList(hotel));

        List<Hotel> result = hotelController.getAllHotels();

        assertEquals(1, result.size());
        assertEquals("Test Hotel", result.get(0).getName());
    }

    @Test
    public void testGetHotelById() {
        Hotel hotel = new Hotel();
        hotel.setName("Test Hotel");
        hotel.setId(1L);

        when(hotelService.getHotelById(1L)).thenReturn(Optional.of(hotel));

        ResponseEntity<Hotel> response = hotelController.getHotelById(1L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Test Hotel", response.getBody().getName());
    }

    @Test
    public void testGetHotelByIdNotFound() {
        when(hotelService.getHotelById(2L)).thenReturn(Optional.empty());

        ResponseEntity<Hotel> response = hotelController.getHotelById(2L);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateHotel() {
        Hotel hotel = new Hotel();
        hotel.setName("Test Hotel");

        when(hotelService.createHotel(any(Hotel.class))).thenReturn(hotel);

        Hotel result = hotelController.createHotel(hotel);

        assertEquals("Test Hotel", result.getName());
    }

    @Test
    public void testUpdateHotel() {
        Hotel existingHotel = new Hotel();
        existingHotel.setId(1L);
        existingHotel.setName("Existing Hotel");
        existingHotel.setAddress("Old Address");

        Hotel updatedHotel = new Hotel();
        updatedHotel.setId(1L);
        updatedHotel.setName("Updated Hotel");
        updatedHotel.setAddress("Old Address");

        Hotel updateInfo = new Hotel();
        updateInfo.setName("Updated Hotel");

        when(hotelService.getHotelById(1L)).thenReturn(Optional.of(existingHotel));
        when(hotelService.updateHotel(eq(1L), any(Hotel.class))).thenReturn(updatedHotel);

        ResponseEntity<Hotel> response = hotelController.updateHotel(1L, updateInfo);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Updated Hotel", response.getBody().getName());
        assertEquals("Old Address", response.getBody().getAddress());

        when(hotelService.getHotelById(2L)).thenReturn(Optional.empty());
        doThrow(new EntityNotFoundException("Hotel not found with id: 2"))
                .when(hotelService).updateHotel(eq(2L), any(Hotel.class));

        ResponseEntity<Hotel> notFoundResponse = hotelController.updateHotel(2L, updateInfo);

        assertEquals(HttpStatus.NOT_FOUND, notFoundResponse.getStatusCode());
    }

    @Test
    public void testDeleteHotel_ExistingHotel() {
        Long testId = 1L;
        Hotel mockHotel = new Hotel();
        mockHotel.setName("Test Hotel");

        when(hotelService.getHotelById(testId)).thenReturn(Optional.of(mockHotel));
        doNothing().when(hotelService).deleteHotel(testId);

        ResponseEntity<Void> response = hotelController.deleteHotel(testId);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(hotelService).deleteHotel(testId);
    }

    @Test
    public void testDeleteHotel_NonExistingHotel() {
        Long testId = 1L;

        when(hotelService.getHotelById(testId)).thenReturn(Optional.empty());

        ResponseEntity<Void> response = hotelController.deleteHotel(testId);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(hotelService, never()).deleteHotel(anyLong());
    }
}