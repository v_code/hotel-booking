package com.hotelbooking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hotelbooking.model.User;
import com.hotelbooking.model.UserRole;
import com.hotelbooking.service.UserService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void getAllUsers_ReturnsListOfUsers() throws Exception {
        User user1 = new User();
        user1.setId(1L);
        user1.setUsername("user1");
        user1.setPassword("password1");
        user1.setEmail("user1@example.com");
        user1.setRole(UserRole.GUEST);

        User user2 = new User();
        user2.setId(2L);
        user2.setUsername("user2");
        user2.setPassword("password2");
        user2.setEmail("user2@example.com");
        user2.setRole(UserRole.HOTEL_OWNER);

        List<User> users = Arrays.asList(user1, user2);

        when(userService.getAllUsers()).thenReturn(users);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].username", is("user1")))
                .andExpect(jsonPath("$[1].username", is("user2")));
    }

    @Test
    public void getUserById_UserExists_ReturnsOk() throws Exception {
        User testUser = new User();
        testUser.setId(1L);
        testUser.setUsername("testUser");
        testUser.setPassword("testPassword");
        testUser.setEmail("test@example.com");
        testUser.setRole(UserRole.GUEST);

        when(userService.getUserById(1L)).thenReturn(Optional.of(testUser));

        mockMvc.perform(get("/users/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("testUser")));
    }

    @Test
    public void getUserById_UserDoesNotExist_ReturnsNotFound() throws Exception {
        when(userService.getUserById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/users/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getUserById_DataAccessException_ReturnsInternalServerError() throws Exception {
        when(userService.getUserById(1L)).thenThrow(new DataIntegrityViolationException("Database error"));

        mockMvc.perform(get("/users/1"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void getUserByUsername_WhenUserExists_ShouldReturnUser() throws Exception {
        User testUser = new User();
        testUser.setId(1L);
        testUser.setUsername("testUser");
        testUser.setPassword("testPassword");
        testUser.setEmail("test@example.com");
        testUser.setRole(UserRole.GUEST);

        when(userService.getUserByUsername("testUser")).thenReturn(Optional.of(testUser));

        mockMvc.perform(get("/users/by-username/testUser"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("testUser")))
                .andExpect(jsonPath("$.email", is("test@example.com")))
                .andExpect(jsonPath("$.role", is("GUEST")));
    }

    @Test
    public void getUserByUsername_WhenUserNotExists_ShouldReturnNotFound() throws Exception {
        when(userService.getUserByUsername("nonexistentUser")).thenReturn(Optional.empty());

        mockMvc.perform(get("/users/by-username/nonexistentUser"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getUserByEmail_WhenUserExists_ShouldReturnUser() throws Exception {
        User testUser = new User();
        testUser.setId(1L);
        testUser.setUsername("testUser");
        testUser.setPassword("testPassword");
        testUser.setEmail("test@example.com");
        testUser.setRole(UserRole.GUEST);

        when(userService.getUserByEmail("test@example.com")).thenReturn(Optional.of(testUser));

        mockMvc.perform(get("/users/by-email/test@example.com"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("testUser")))
                .andExpect(jsonPath("$.email", is("test@example.com")))
                .andExpect(jsonPath("$.role", is("GUEST")));
    }

    @Test
    public void getUserByEmail_WhenUserNotExists_ShouldReturnNotFound() throws Exception {
        when(userService.getUserByEmail("nonexistent@example.com")).thenReturn(Optional.empty());

        mockMvc.perform(get("/users/by-email/nonexistent@example.com"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createUser_ReturnsUser() throws Exception {
        User testUser = new User();
        testUser.setId(1L);
        testUser.setUsername("testUser");
        testUser.setPassword("testPassword");
        testUser.setEmail("test@example.com");
        testUser.setRole(UserRole.GUEST);

        when(userService.saveUser(any(User.class))).thenReturn(testUser);

        mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testUser)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("testUser")))
                .andExpect(jsonPath("$.email", is("test@example.com")))
                .andExpect(jsonPath("$.role", is("GUEST")));
    }

    @Test
    public void updateUser_WhenUserExists_ShouldUpdateAndReturnUser() throws Exception {
        User existingUser = new User();
        existingUser.setId(1L);
        existingUser.setUsername("existingUser");
        existingUser.setPassword("existingPassword");
        existingUser.setEmail("existing@example.com");
        existingUser.setRole(UserRole.GUEST);

        User updatedUser = new User();
        updatedUser.setId(1L);
        updatedUser.setUsername("updatedUser");
        updatedUser.setPassword("updatedPassword");
        updatedUser.setEmail("updated@example.com");
        updatedUser.setRole(UserRole.GUEST);

        when(userService.updateUser(1L, existingUser)).thenReturn(updatedUser);

        mockMvc.perform(put("/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(existingUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("updatedUser")));
    }

    @Test
    public void updateUser_WhenUserDoesNotExist_ShouldReturnNotFound() throws Exception {
        User nonExistingUser = new User();
        nonExistingUser.setId(2L);
        nonExistingUser.setUsername("nonExisting");
        nonExistingUser.setPassword("nonExistingPassword");
        nonExistingUser.setEmail("nonExisting@example.com");
        nonExistingUser.setRole(UserRole.GUEST);

        when(userService.updateUser(2L, nonExistingUser)).thenThrow(new EntityNotFoundException("User with id 2 not found"));

        mockMvc.perform(put("/users/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(nonExistingUser)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteUser_UserExists_ReturnsNoContent() throws Exception {
        doNothing().when(userService).deleteUser(1L);

        mockMvc.perform(delete("/users/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteUser_UserDoesNotExist_ReturnsNotFound() throws Exception {
        doThrow(EmptyResultDataAccessException.class).when(userService).deleteUser(1L);

        mockMvc.perform(delete("/users/1"))
                .andExpect(status().isNotFound());
    }

}
