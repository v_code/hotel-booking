package com.hotelbooking.controller;

import com.hotelbooking.model.Review;
import com.hotelbooking.service.ReviewService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ReviewControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ReviewService reviewService;

    @InjectMocks
    private ReviewController reviewController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(reviewController).build();
    }

    @Test
    public void getAllReviews_ReturnsListOfReviews() throws Exception {
        Review review1 = new Review();
        review1.setId(1L);
        review1.setRating(5);
        review1.setComment("Review1");
        review1.setDate(LocalDate.now());

        Review review2 = new Review();
        review2.setId(2L);
        review2.setRating(4);
        review2.setComment("Review2");
        review2.setDate(LocalDate.now());

        List<Review> mockReviews = Arrays.asList(review1, review2);
        when(reviewService.getAllReviews()).thenReturn(mockReviews);

        mockMvc.perform(get("/reviews"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].rating", is(5)))
                .andExpect(jsonPath("$[0].comment", is("Review1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].rating", is(4)))
                .andExpect(jsonPath("$[1].comment", is("Review2")));

        verify(reviewService, times(1)).getAllReviews();
    }

    @Test
    public void getReviewById_ReturnsReview_WhenFound() throws Exception {
        Review mockReview = new Review();
        mockReview.setId(1L);
        mockReview.setRating(5);
        mockReview.setComment("Review1");
        mockReview.setDate(LocalDate.now());

        when(reviewService.getReviewById(1L)).thenReturn(Optional.of(mockReview));

        mockMvc.perform(get("/reviews/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.rating", is(5)))
                .andExpect(jsonPath("$.comment", is("Review1")));

        verify(reviewService, times(1)).getReviewById(1L);
    }

    @Test
    public void getReviewById_ReturnsNotFound_WhenNotPresent() throws Exception {
        when(reviewService.getReviewById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/reviews/1"))
                .andExpect(status().isNotFound());

        verify(reviewService, times(1)).getReviewById(1L);
    }

    @Test
    public void testCreateReview() throws Exception {
        Review review = new Review();
        review.setId(1L);
        review.setRating(5);
        review.setComment("Great room!");

        when(reviewService.saveReview(any(Review.class))).thenReturn(review);

        String reviewJson = "{\"rating\":5,\"comment\":\"Great room!\"}";

        mockMvc.perform(post("/reviews")
                        .contentType(APPLICATION_JSON)
                        .content(reviewJson))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"rating\":5,\"comment\":\"Great room!\"}"));

        verify(reviewService, times(1)).saveReview(any(Review.class));
    }

    @Test
    public void deleteReview_ReturnsNoContent_WhenReviewExists() throws Exception {
        Review mockReview = new Review();
        mockReview.setId(1L);
        mockReview.setRating(5);
        mockReview.setComment("Review1");
        mockReview.setDate(LocalDate.now());

        when(reviewService.getReviewById(1L)).thenReturn(Optional.of(mockReview));

        mockMvc.perform(delete("/reviews/1"))
                .andExpect(status().isNoContent());

        verify(reviewService, times(1)).deleteReview(1L);
    }

    @Test
    public void updateReview_ReturnsUpdatedReview_WhenReviewExists() throws Exception {
        Review existingReview = new Review();
        existingReview.setId(1L);
        existingReview.setRating(5);
        existingReview.setComment("Original Review");
        existingReview.setDate(LocalDate.now());

        Review updatedReview = new Review();
        updatedReview.setId(1L);
        updatedReview.setRating(4);
        updatedReview.setComment("Updated Review");
        updatedReview.setDate(LocalDate.now());

        when(reviewService.getReviewById(1L)).thenReturn(Optional.of(existingReview));
        when(reviewService.saveReview(any(Review.class))).thenReturn(updatedReview);

        String reviewJson = "{\"rating\":4,\"comment\":\"Updated Review\"}";

        mockMvc.perform(put("/reviews/1")
                        .contentType(APPLICATION_JSON)
                        .content(reviewJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.rating", is(4)))
                .andExpect(jsonPath("$.comment", is("Updated Review")));

        verify(reviewService, times(1)).saveReview(any(Review.class));
    }

    @Test
    public void updateReview_ReturnsNotFound_WhenReviewDoesNotExist() throws Exception {
        when(reviewService.getReviewById(1L)).thenReturn(Optional.empty());

        String reviewJson = "{\"rating\":4,\"comment\":\"Updated Review\"}";

        mockMvc.perform(put("/reviews/1")
                        .contentType(APPLICATION_JSON)
                        .content(reviewJson))
                .andExpect(status().isNotFound());

        verify(reviewService, times(0)).saveReview(any(Review.class));
    }

    @Test
    public void deleteReview_ReturnsNotFound_WhenReviewDoesNotExist() throws Exception {
        when(reviewService.getReviewById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(delete("/reviews/1"))
                .andExpect(status().isNotFound());

        verify(reviewService, times(0)).deleteReview(1L);
    }

}
