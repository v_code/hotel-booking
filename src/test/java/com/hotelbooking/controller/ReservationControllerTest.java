package com.hotelbooking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.hotelbooking.model.Reservation;
import com.hotelbooking.model.ReservationStatus;
import com.hotelbooking.service.ReservationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReservationController.class)
public class ReservationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReservationService reservationService;

    @Test
    public void getAllReservations_ReturnsListOfReservations() throws Exception {
        List<Reservation> reservationList = new ArrayList<>();
        Reservation testReservation = new Reservation();
        reservationList.add(testReservation);

        when(reservationService.getAllReservations()).thenReturn(reservationList);

        mockMvc.perform(get("/reservations"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void getAllReservations_ReturnsNoContentWhenNoReservations() throws Exception {
        when(reservationService.getAllReservations()).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/reservations"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getReservationById_ExistingId_ReturnsReservation() throws Exception {
        Reservation testReservation = new Reservation();
        testReservation.setId(1L);

        when(reservationService.getReservationById(1L)).thenReturn(Optional.of(testReservation));

        mockMvc.perform(get("/reservations/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void getReservationById_NonExistingId_ReturnsNotFound() throws Exception {
        when(reservationService.getReservationById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/reservations/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createReservation_ReturnsReservation() throws Exception {
        Reservation testReservation = new Reservation();
        testReservation.setId(1L);
        testReservation.setStartDate(LocalDate.of(2023, 10, 10));
        testReservation.setEndDate(LocalDate.of(2023, 10, 20));
        testReservation.setStatus(ReservationStatus.ACTIVE);

        when(reservationService.saveReservation(any(Reservation.class))).thenReturn(testReservation);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        mockMvc.perform(post("/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(testReservation)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.startDate", is("2023-10-10")))
                .andExpect(jsonPath("$.endDate", is("2023-10-20")))
                .andExpect(jsonPath("$.status", is("ACTIVE")));
    }

    @Test
    public void deleteReservation_WhenReservationExists_ReturnsNoContent() throws Exception {
        Long testId = 1L;

        Reservation testReservation = new Reservation();
        testReservation.setId(testId);

        when(reservationService.getReservationById(testId)).thenReturn(Optional.of(testReservation));

        mockMvc.perform(delete("/reservations/" + testId))
                .andExpect(status().isNoContent());

        verify(reservationService).deleteReservation(testId);
    }

    @Test
    public void updateReservation_ReturnsUpdatedReservation() throws Exception {
        Reservation originalReservation = new Reservation();
        originalReservation.setId(1L);
        originalReservation.setStartDate(LocalDate.of(2023, 10, 10));
        originalReservation.setEndDate(LocalDate.of(2023, 10, 20));
        originalReservation.setStatus(ReservationStatus.ACTIVE);

        Reservation updatedReservation = new Reservation();
        updatedReservation.setId(1L);
        updatedReservation.setStartDate(LocalDate.of(2023, 10, 15));
        updatedReservation.setEndDate(LocalDate.of(2023, 10, 25));
        updatedReservation.setStatus(ReservationStatus.CANCELED);

        when(reservationService.getReservationById(1L)).thenReturn(Optional.of(originalReservation));
        when(reservationService.saveReservation(updatedReservation)).thenReturn(updatedReservation);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        mockMvc.perform(put("/reservations/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedReservation)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.startDate", is("2023-10-15")))
                .andExpect(jsonPath("$.endDate", is("2023-10-25")))
                .andExpect(jsonPath("$.status", is("CANCELED")));
    }


    @Test
    public void deleteReservation_WhenReservationNotFound_ReturnsNotFound() throws Exception {
        Long testId = 1L;

        when(reservationService.getReservationById(testId)).thenReturn(Optional.empty());

        mockMvc.perform(delete("/reservations/" + testId))
                .andExpect(status().isNotFound());

        verify(reservationService, never()).deleteReservation(testId);
    }
}