package com.hotelbooking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hotelbooking.model.Room;
import com.hotelbooking.service.RoomService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.is;

@WebMvcTest(RoomController.class)
public class RoomControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RoomService roomService;

    @BeforeEach
    public void setup(WebApplicationContext wac) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void getAllRooms_ShouldReturnListOfRooms() throws Exception {
        Room room1 = new Room();
        room1.setId(1L);
        Room room2 = new Room();
        room2.setId(2L);
        List<Room> expectedRooms = Arrays.asList(room1, room2);

        when(roomService.getAllRooms()).thenReturn(expectedRooms);

        mockMvc.perform(get("/rooms"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{'id':1},{'id':2}]"));
    }

    @Test
    public void testGetRoomById_Exists() throws Exception {
        Room room = new Room();
        room.setId(1L);
        room.setNumberRoom(101);

        when(roomService.getRoomById(1L)).thenReturn(Optional.of(room));

        mockMvc.perform(get("/rooms/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.numberRoom", is(101)));
    }

    @Test
    public void testGetRoomById_NotExists() throws Exception {
        when(roomService.getRoomById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/rooms/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createRoom_ShouldReturnCreatedRoom() throws Exception {
        Room room = new Room();
        room.setId(1L);
        room.setNumberRoom(101);

        when(roomService.saveRoom(room)).thenReturn(room);

        ObjectMapper objectMapper = new ObjectMapper();
        String roomJson = objectMapper.writeValueAsString(room);

        mockMvc.perform(post("/rooms")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(roomJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.numberRoom").value(101));
    }

    @Test
    public void updateRoom_ShouldReturnUpdatedRoom_WhenRoomExists() throws Exception {
        Room roomToUpdate = new Room();
        roomToUpdate.setId(1L);
        roomToUpdate.setNumberRoom(101);
        roomToUpdate.setType("Deluxe");
        roomToUpdate.setPrice(new BigDecimal("150.00"));
        roomToUpdate.setDescription("Updated description");

        Room updatedRoom = new Room();
        updatedRoom.setId(1L);
        updatedRoom.setNumberRoom(101);
        updatedRoom.setType("Deluxe");
        updatedRoom.setPrice(new BigDecimal("150.00"));
        updatedRoom.setDescription("Updated description");

        when(roomService.updateRoom(anyLong(), any(Room.class))).thenReturn(updatedRoom);

        mockMvc.perform(put("/rooms/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(roomToUpdate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description", is("Updated description")));

        verify(roomService).updateRoom(eq(1L), any(Room.class));
    }

    @Test
    public void updateRoom_ShouldReturnNotFound_WhenRoomDoesNotExist() throws Exception {
        Room roomToUpdate = new Room();
        roomToUpdate.setId(2L);
        roomToUpdate.setNumberRoom(202);

        when(roomService.updateRoom(anyLong(), any(Room.class))).thenThrow(new EntityNotFoundException("Room not found"));

        mockMvc.perform(put("/rooms/{id}", 2L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(roomToUpdate)))
                .andExpect(status().isNotFound());

        verify(roomService).updateRoom(eq(2L), any(Room.class));
    }

    @Test
    public void deleteRoom_RoomExists_ReturnsNoContent() throws Exception {
        Long existingRoomId = 1L;
        Optional<Room> roomOptional = Optional.of(new Room());
        Mockito.when(roomService.getRoomById(existingRoomId)).thenReturn(roomOptional);

        mockMvc.perform(delete("/rooms/{id}", existingRoomId))
                .andExpect(status().isNoContent());

        verify(roomService, times(1)).deleteRoom(existingRoomId);
    }

    @Test
    public void deleteRoom_RoomDoesNotExist_ReturnsNotFound() throws Exception {
        Long nonExistingRoomId = 2L;
        Mockito.when(roomService.getRoomById(nonExistingRoomId)).thenReturn(Optional.empty());

        mockMvc.perform(delete("/rooms/{id}", nonExistingRoomId))
                .andExpect(status().isNotFound());

        verify(roomService, times(0)).deleteRoom(nonExistingRoomId);
    }
}