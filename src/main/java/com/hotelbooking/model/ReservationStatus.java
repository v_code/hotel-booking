package com.hotelbooking.model;

public enum ReservationStatus {
    ACTIVE,
    COMPLETED,
    CANCELED
}
