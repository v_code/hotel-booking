package com.hotelbooking.model;

public enum UserRole {
    GUEST,
    HOTEL_OWNER
}
