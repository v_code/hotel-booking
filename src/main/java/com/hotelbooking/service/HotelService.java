package com.hotelbooking.service;

import com.hotelbooking.model.Hotel;
import com.hotelbooking.repository.HotelRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HotelService {

    private final HotelRepository hotelRepository;

    @Autowired
    public HotelService(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    public List<Hotel> getAllHotels() {
        return hotelRepository.findAllByOrderByIdAsc();
    }

    public Optional<Hotel> getHotelById(Long id) {
        return hotelRepository.findById(id);
    }

    public Hotel createHotel(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    public Hotel updateHotel(Long id, Hotel updatedHotel) {
        return hotelRepository.findById(id)
                .map(existingHotel -> {
                    if (updatedHotel.getName() != null) {
                        existingHotel.setName(updatedHotel.getName());
                    }
                    if (updatedHotel.getAddress() != null) {
                        existingHotel.setAddress(updatedHotel.getAddress());
                    }
                    if (updatedHotel.getCity() != null) {
                        existingHotel.setCity(updatedHotel.getCity());
                    }
                    if (updatedHotel.getDescription() != null) {
                        existingHotel.setDescription(updatedHotel.getDescription());
                    }
                    if (updatedHotel.getRating() != null) {
                        existingHotel.setRating(updatedHotel.getRating());
                    }
                    return hotelRepository.save(existingHotel);
                })
                .orElseThrow(() -> new EntityNotFoundException("Hotel not found with id: " + id));
    }

    public void deleteHotel(Long id) {
        hotelRepository.deleteById(id);
    }
}