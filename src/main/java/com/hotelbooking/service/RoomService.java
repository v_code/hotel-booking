package com.hotelbooking.service;

import com.hotelbooking.model.Hotel;
import com.hotelbooking.model.Room;
import com.hotelbooking.repository.HotelRepository;
import com.hotelbooking.repository.RoomRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final HotelRepository hotelRepository;

    public RoomService(RoomRepository roomRepository, HotelRepository hotelRepository) {
        this.roomRepository = roomRepository;
        this.hotelRepository = hotelRepository;
    }

    public List<Room> getAllRooms() {
        return roomRepository.findAllByOrderByIdAsc();
    }

    public Optional<Room> getRoomById(Long id) {
        return roomRepository.findById(id);
    }

    public Room saveRoom(Room room) {
        Long hotelId = room.getHotel().getId();
        Hotel hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new EntityNotFoundException("Hotel not found with id: " + hotelId));

        room.setHotel(hotel);
        return roomRepository.save(room);
    }

    @Transactional
    public Room updateRoom(Long id, Room room) {
        Room existingRoom = roomRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Room not found with id: " + id));

        if (room.getHotel() != null && room.getHotel().getId() != null) {
            Hotel hotel = hotelRepository.findById(room.getHotel().getId())
                    .orElseThrow(() -> new EntityNotFoundException("Hotel not found with id: " + room.getHotel().getId()));
            existingRoom.setHotel(hotel);
        }

        if (room.getNumberRoom() != null) existingRoom.setNumberRoom(room.getNumberRoom());
        if (room.getType() != null) existingRoom.setType(room.getType());
        if (room.getPrice() != null) existingRoom.setPrice(room.getPrice());
        if (room.getDescription() != null) existingRoom.setDescription(room.getDescription());

        return roomRepository.save(existingRoom);
    }

    @Transactional
    public void deleteRoom(Long id) {
        if (!roomRepository.existsById(id)) {
            throw new EntityNotFoundException("Room not found with id: " + id);
        }
        roomRepository.deleteById(id);
    }
}